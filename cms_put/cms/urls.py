from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('main.css', views.style, name='style'),
    path('update-content', views.update_content, name='update_content'),  # Añade la ruta para PUT
]
