# cms/models.py
from django.db import models

class Content(models.Model):
    resource = models.CharField(max_length=100, unique=True)
    content = models.TextField()

    def __str__(self):
        return self.resource
