# cms/views.py
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
import random
import json
from django.views.decorators.csrf import csrf_exempt
from .models import Content

def index(request):
    return render(request, 'index.html')

def style(request):
    colors = ["blue", "gold", "red", "aqua", "green", "black", "yellow"]
    color = random.choice(colors)
    background = random.choice(colors)

    css_content = f"""
    body {{
        margin: 10px 20% 50px 70px;
        font-family: sans-serif;
        color: {color};
        background: {background};
    }}
    """
    return HttpResponse(css_content, content_type="text/css")

@csrf_exempt
def update_content(request):
    if request.method == 'PUT':
        data = json.loads(request.body)
        resource = data.get('resource')
        content = data.get('content')

        if resource and content:
            # Actualizar o crear el contenido en la base de datos
            obj, created = Content.objects.update_or_create(resource=resource, defaults={'content': content})
            return JsonResponse({'status': 'success', 'created': created})

        return JsonResponse({'status': 'error', 'message': 'Invalid data'}, status=400)
    return JsonResponse({'status': 'error', 'message': 'Invalid method'}, status=405)
